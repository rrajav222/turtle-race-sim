from turtle import Turtle, Screen
import random

screen = Screen()
screen.setup(width=700, height=500)
colors = ['violet', 'indigo', 'blue', 'green', 'yellow', 'orange', 'red']


def turtle_race():
    start_x = -325
    start_y = - 150

    screen.clear()
    turtles_list = []

    for item in colors:
        turtle = Turtle()
        turtle.shape('turtle')
        turtle.color(item)
        turtles_list.append(turtle)
        turtle.up()
        turtle.goto(-325, start_y)
        start_y += 50

    # finish line
    line = Turtle()
    line.up()
    line.goto(325, -200)
    line.setheading(270)
    line.down()
    line.goto(325, 200)
    line.up()

    user_choice = screen.textinput(
        "Who will win?", "Which turtle do you think is going to win?")

    game_over = False
    winner_list = []

    while not game_over:
        for each_turtle in turtles_list:
            each_turtle.fd(random.randint(5, 20))
            if each_turtle.xcor() >= 310.0:
                winner_list.append(each_turtle)
                game_over = True

    max = 0
    for item in winner_list:
        print(item.fillcolor(), item.xcor())
        if item.xcor() > max:
            max = item.xcor()
            winner = item
        print(winner.fillcolor(), winner.xcor())

    if user_choice == winner.fillcolor():
        again = screen.textinput(
            "You have won", f"""{user_choice} has won!!
            Want to play again? (Y/N)""")
    else:
        again = screen.textinput(
            "You have lost", f"""{winner.fillcolor()} was the winner :(
            Want to play again? (Y/N)""")

    if again.upper() == "Y":
        turtle_race()
    else:
        screen.bye()


turtle_race()
# screen.exitonclick()
